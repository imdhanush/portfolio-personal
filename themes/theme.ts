import { extendTheme } from '@chakra-ui/react';

const colors = {
  brand: {
    900: '#1a365d',
    800: '#153e75',
    700: '#2a69ac',
  },
};

export const customTheme = extendTheme({
  components: {
    Divider: {
      variants: {
        'black-custom': {
          bg: 'black',
          width: '1px',
          border: '1px solid #000000',
        },
      },
    },
  },
  colors,
  textStyles: {
    navBarText: {
      fontWeight: '500',
      fontSize: '20px',
      fontStyle: 'normal',
      lineHeight: '30px',
      textAlign: 'right',
    },
    headerText: {
      fontWeight: '700',
      fontSize: '44px',
      fontStyle: 'normal',
      lineHeight: '60px',
    },
    cardTitleText: {
      fontWeight: '700',
      fontSize: '26px',
      fontStyle: 'normal',
      lineHeight: '40px',
    },
    cardTypeText: {
      fontWeight: '400',
      fontSize: '18px',
      fontStyle: 'normal',
      lineHeight: '27px',
    },
    cardContentText: {
      fontWeight: '400',
      fontSize: '16px',
      fontStyle: 'normal',
      lineHeight: '24px',
    },
    RecentPostText: {
      fontWeight: '400',
      fontSize: '22px',
      fontStyle: 'normal',
      lineHeight: '60px',
    },
    FeaturedWorksTitleText: {
      fontWeight: 'bold',
      fontSize: '30px',
      fontStyle: 'normal',
      lineHeight: '44px',
    },
    TagText: {
      fontWeight: '900',
      fontSize: '18px',
      fontStyle: 'normal',
      lineHeight: '60px',
    },
    FeatureWorksTypeText: {
      fontWeight: 'normal',
      fontSize: '20px',
      fontStyle: 'normal',
      lineHeight: '29px',
    },
    CopyrightText: {
      fontWeight: 'normal',
      fontSize: '14px',
      fontStyle: 'normal',
      lineHeight: '21px',
    },
    blogTitleText: {
      fontWeight: '500',
      fontSize: '30px',
      lineHeight: '44.06px',
      fontStyle: 'normal',
    },
    blogCardLightText: {
      fontWeight: '400',
      fontSize: '20px',
      lineHeight: '29px',
      fontStyle: 'normal',
      color: '#8695A4',
    },
  },
});
