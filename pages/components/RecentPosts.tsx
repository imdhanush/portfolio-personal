import { Box, HStack, Link, VStack } from '@chakra-ui/react';
import React from 'react';
import PostCardsComponent from './common/PostCards';

export default function RecentPostsComponent(): JSX.Element {
  return (
    <Box padding="6px 25%" mt="88px" h="400px" w="100%" bgColor="#EDF7FA">
      <HStack justifyContent="space-between" w="100%">
        <Box textStyle="RecentPostText">Recent Posts</Box>
        <Link
          href="https://imdhanush.medium.com"
          color="#00A8CC"
          textStyle="cardContentText"
          isExternal
        >
          View all
        </Link>
      </HStack>
      <HStack w="100%" justifyContent="space-between">
        <PostCardsComponent
          title="Making a design system from scratch"
          content="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet."
          date="12 Feb 2020"
          type="Design, Pattern"
        />
        <PostCardsComponent
          title="Creating pixel perfect icons in Figma"
          content="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet."
          date="12 Feb 2020"
          type="Figma, Icon Design"
        />
      </HStack>
    </Box>
  );
}
