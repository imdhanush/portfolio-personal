import { Box, HStack, Link } from '@chakra-ui/react';
import { useRouter } from 'next/router';

export default function NavHeader(): JSX.Element {
  const router = useRouter();
  const path = router.pathname.split('/');
  return (
    <HStack spacing="33px" justifyContent="flex-end" padding="2rem 2rem">
      <Link style={{ textDecoration: 'none' }} href="/works">
        <Box
          color={path?.[1] === 'works' ? '#FF6464' : 'black'}
          textStyle="navBarText"
        >
          Works
        </Box>
      </Link>
      <Link style={{ textDecoration: 'none' }} href="/blog">
        <Box
          color={path?.[1] === 'blog' ? '#FF6464' : 'black'}
          style={{ cursor: 'pointer' }}
          textStyle="navBarText"
        >
          Blog
        </Box>
      </Link>
      {/* <Link style={{ textDecoration: 'none' }} href="/contact">
        <Box
          color={path?.[1] === 'contact' ? '#FF6464' : 'black'}
          textStyle="navBarText"
        >
          Contact
        </Box>
      </Link> */}
    </HStack>
  );
}
