import { HStack, Container, Box, Text, Button, Image } from '@chakra-ui/react';
import styles from '../../styles/Home.module.css';

export default function HomeIntroComponent(): JSX.Element {
  return (
    <HStack mt="56" justifyContent="space-between" p="0 200px">
      <Container width="min-content" m="0" maxW="none" w="">
        <Box w="506px" textStyle="headerText">
          Hi, I am Dhanush,
        </Box>
        <Box textStyle="headerText"> Fullstack Developer</Box>
        <Text mt="40px">
          Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
          sint. Velit officia consequat duis enim velit mollit. Exercitation
          veniam consequat sunt nostrud amet.{' '}
        </Text>
        <Button
          mt="47px"
          w="208px"
          h="47px"
          bg="#FF6464"
          color="white"
          fontWeight="500"
          fontStyle="normal"
          lineHeight="30px"
          fontSize="20px"
        >
          Download Resume
        </Button>
      </Container>
      <>
        <Image
          className={styles.pImage}
          src="/tswamy.jpeg"
          width="243"
          height="243"
          alt="Profile pic"
        />
      </>
    </HStack>
  );
}
