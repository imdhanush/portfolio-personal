import { Container, Text, HStack, Divider } from '@chakra-ui/react';

interface BlogDataInterface {
  title?: string;
  date?: string;
  subTitle?: string;
  description?: string;
}

const BlogCard = (props: { data: BlogDataInterface }): JSX.Element => {
  const { data } = props;
  return (
    <>
      <Text textStyle="blogTitleText">{data.title}</Text>
      <HStack mt="16px">
        <Text>{data.date}</Text>
        <Divider orientation="vertical" h="21px" variant="black-custom" />
        <Text textStyle="blogCardLightText">{data.subTitle}</Text>
      </HStack>
      <Text mt="6px">{data.description}</Text>
      <Divider mt="29px" mb="32px" orientation="horizontal" />
    </>
  );
};

export default function BlogComponent(): JSX.Element {
  const blogsData: BlogDataInterface[] = [
    {
      title: 'UI Interactions of the week',
      date: '12-Feb-2019',
      subTitle: 'Express, Handlebars',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
    },
    {
      title: 'UI Interactions of the week',
      date: '12-Feb-2019',
      subTitle: 'Express, Handlebars',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
    },
    {
      title: 'UI Interactions of the week',
      date: '12-Feb-2019',
      subTitle: 'Express, Handlebars',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
    },
    {
      title: 'UI Interactions of the week',
      date: '12-Feb-2019',
      subTitle: 'Express, Handlebars',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
    },
  ];
  return (
    <Container>
      <Text mb="55px" textStyle="headerText">
        Blog
      </Text>
      {blogsData.map((item: BlogDataInterface, index: number) => (
        <BlogCard key={index} data={item} />
      ))}
    </Container>
  );
}
