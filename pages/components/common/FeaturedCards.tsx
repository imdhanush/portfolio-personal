import { HStack, Image, Tag, VStack, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';

export default function FeaturedCards(data: {
  imageSrc?: string;
  title?: string;
  imageAlt?: string;
  type?: string;
  year?: string;
  description?: string;
  index: number;
}): JSX.Element {
  const router = useRouter();
  return (
    <HStack
      cursor="pointer"
      onClick={() => {
        router.push('/detailed_work/' + data.index);
      }}
    >
      <Image w="246px" h="180px" src={data.imageSrc} alt={data.imageSrc} />
      <VStack>
        <Text textStyle="FeaturedWorksTitleText" alignSelf="flex-start">
          {data.title}
        </Text>
        <HStack spacing="36px" alignSelf="flex-start">
          <Tag textStyle="TagText" bgColor="black" color="white">
            {data.year}
          </Tag>
          <Text color="#8695A4" textStyle="FeatureWorksTypeText">
            {data.type}
          </Text>
        </HStack>
        <Text textStyle="cardContentText">{data.description}</Text>
      </VStack>
    </HStack>
  );
}
