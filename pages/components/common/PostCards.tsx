import { Box, Divider, HStack } from '@chakra-ui/react';

export default function PostCardsComponent(data: {
  title?: string;
  date?: string;
  content?: string;
  type?: string;
}): JSX.Element {
  return (
    <Box
      p="24px"
      w="418px"
      h="295px"
      bgColor="white"
      borderRadius="4px"
      boxShadow="0px 4px 10px rgba(187, 225, 250, 0.25);"
    >
      <Box textStyle="cardTitleText">{data.title}</Box>
      <HStack mt="17px" mb="11px" spacing="26px">
        <Box textStyle="cardTypeText">{data.date}</Box>
        <Divider borderLeftWidth="2px" orientation="vertical" h="21px" />
        <Box alignSelf="center" textStyle="cardContentText">
          {data.type}
        </Box>
      </HStack>
      <Box>{data.content}</Box>
    </Box>
  );
}
