import { Container, Text } from '@chakra-ui/react';

interface DetailedWorkProps {
  heading: string;
}

const DetailedWorkComponent = (props: {
  data: DetailedWorkProps;
}): JSX.Element => {
  return (
    <Container>
      <Text>Works</Text>
    </Container>
  );
};

export default DetailedWorkComponent;
