import { Divider, Text, VStack } from '@chakra-ui/react';
import FeaturedCards from './common/FeaturedCards';

export default function FeaturedWorks(): JSX.Element {
  const featuredWorksData = [
    {
      year: '2020',
      title: 'Designing Dashboards',
      imageAlt: 'Dashboard',
      imageSrc: '/dashboard.png',
      type: 'Dashboard',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
    },
    {
      year: '2018',
      title: 'Vibrant Portraits of 2020',
      imageAlt: 'Dashboard',
      imageSrc: '/vibrant.png',
      type: 'Illustration',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
    },
    {
      year: '2018',
      title: '36 Days of Malayalam type',
      imageAlt: 'Dashboard',
      imageSrc: '/days.png',
      type: 'Typography',
      description:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
    },
  ];
  return (
    <VStack padding="6px 25%" mt="88px" spacing="46px">
      <Text alignSelf="flex-start" textStyle="headerText">
        Works
      </Text>
      {featuredWorksData.map((val, ind) => (
        <FeaturedCards
          key={ind}
          year={val.year}
          title={val.title}
          imageAlt={val.imageAlt}
          imageSrc={val.imageSrc}
          type={val.type}
          description={val.description}
          index={ind + 1}
        />
      ))}
      <Divider orientation="horizontal" />
    </VStack>
  );
}
