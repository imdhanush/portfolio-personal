import { HStack, Image, VStack, Text } from '@chakra-ui/react';

export default function Footer(): JSX.Element {
  return (
    <VStack padding="6px 25%" spacing="1px">
      <HStack mt="83px" height="182px" w="100%" justifyContent="center">
        <Image src="/fb.svg" alt="Facebook" />
        <Image src="/insta.svg" alt="Instagram" />
        <Image src="/twitter.svg" alt="Twitter" />
        <Image src="/linkedin.svg" alt="LinkedIn" />
      </HStack>
      <Text textStyle="CopyrightText">
        Copyright ©2020 All rights reserved{' '}
      </Text>
    </VStack>
  );
}
