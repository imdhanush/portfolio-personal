import Head from 'next/head';
import FeaturedWorks from './components/FeaturedWorks';
import HomeIntroComponent from './components/HomeIntro';
import RecentPostsComponent from './components/RecentPosts';

export default function Home(): JSX.Element {
  return (
    <>
      <HomeIntroComponent />
      <RecentPostsComponent />
      <FeaturedWorks />
    </>
  );
}
