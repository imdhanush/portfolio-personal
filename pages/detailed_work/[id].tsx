import { useRouter } from 'next/router';
import DetailedWorkComponent from '../components/DetailedWorks';

const DetailedWork = (): JSX.Element => {
  const { query } = useRouter();
  return <DetailedWorkComponent data={{ heading: 'Works' }} />;
};
export default DetailedWork;
