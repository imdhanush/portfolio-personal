import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { ChakraProvider } from '@chakra-ui/react';
import { customTheme } from '../themes/theme';
import NavHeader from './components/NavHeader';
import Footer from './components/Footer';

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <ChakraProvider theme={customTheme}>
      <NavHeader />
      <Component>{pageProps}</Component>
      <Footer />
    </ChakraProvider>
  );
}
export default MyApp;
